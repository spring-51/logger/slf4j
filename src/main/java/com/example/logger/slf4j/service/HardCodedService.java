package com.example.logger.slf4j.service;

import com.example.logger.slf4j.manager.HardCodedManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class HardCodedService {
    @Autowired
    private HardCodedManager hardCodedManager;

    public void logFromService() {
        log.trace("A TRACE Message");
        log.debug("A DEBUG Message");
        log.info("An INFO Message");
        log.warn("A WARN Message");
        log.error("An ERROR Message");
        hardCodedManager.logFromManager();
    }
}
