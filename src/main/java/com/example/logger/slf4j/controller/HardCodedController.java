package com.example.logger.slf4j.controller;

import com.example.logger.slf4j.service.HardCodedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HardCodedController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private HardCodedService hardCodedService;

    @GetMapping
    public String logFromController(){
        logger.trace("A TRACE Message");
        logger.debug("A DEBUG Message");
        logger.info("An INFO Message");
        logger.warn("A WARN Message");
        logger.error("An ERROR Message");
        hardCodedService.logFromService();
        return "check logs";
    }
}
